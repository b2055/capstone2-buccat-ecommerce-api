const jwt = require('jsonwebtoken')
const secret = "Capstone2"


module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret, {})
}

//Token verification

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization

    if(typeof token !== "underfined") {
        console.log(token)

        //Bearer
        token = token.slice(7, token.length)

        //validate the token using the "verify" method
        return jwt.verify(token, secret, (err, data) => {
            //if jwt is not valid
            if(err) {
                return res.send({auth: "failed"})
            } else {
                next()
            }
        })
    }

    // Token does not exist

    else {
        return res.send({auth: "failed"})
    }
    
}

module.exports.decode = (token) => {
    //token recieved and is not undefiend
    if(typeof token !== "undefined") {
        token = token.slice(7, token.length)

        return jwt.verify(token,secret, (err, data) => {
            if(err){
                return null
            } else {
                // Returns an object with access to the "payload" property which contains user information stored when the token was generated
                return jwt.decode(token, {complete:true}).payload
            }
        })
    } else {
        return null
    }
}