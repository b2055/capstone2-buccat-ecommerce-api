//Main
const Product = require('../models/Product')
//Secondary
const User = require('../models/User')
const auth = require('../auth')

//Get all product
module.exports.getAllProduct = (reqBody) => {
    return Product.find({isActive: true}).then(result => {
        return result
    })
}

//Retrieve a single product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result
    })
}

//Create a new product
/* 
    Steps:
        1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API creates product using the contents of the request body.
*/

module.exports.createProduct = (reqBody, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {

            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })

            return newProduct.save().then((product, error) => {
                return error ? false : 'Successfully created a product';
            })
        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}

//Update a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL 
            parameter and overwrites its info with those from the request body.
*/
module.exports.updateProduct = (reqBody, reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {

            let updatedProduct = {
                name: reqBody.name,
                description:reqBody.description,
                price: reqBody.price
            }
        
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : 'Successfully updated a product';
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}

//Archive a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.
*/
module.exports.archiveProduct = (reqBody, reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {

            let updatedProduct = {
                isActive: false
            }
        
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : 'Successfully archived a product';
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}

//Unarchive a product
/* 
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/unarchive endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to true.
*/
module.exports.unarchiveProduct = (reqBody, reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {

            let updatedProduct = {
                isActive: true
            }
        
            return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
                return error ? false : 'Successfully unarchived a product';
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}
