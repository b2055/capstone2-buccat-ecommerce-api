//Main
const Order = require('../models/Order')
//Secondary
const User = require('../models/User')
const Product = require('../models/Product')
const auth = require('../auth')
const { response } = require('express')
const { findById } = require('../models/Product')

/* 
Retrieve All Orders
1. A GET request containing a JWT in its header is sent to the /users/orders endpoint.
2. API validates user is an admin via JWT, returns false if validation fails.
3. If validation is successful, API retrieves all orders and returns them in its response.
*/
module.exports.getAllOrder = (userData) => {
    
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Order.find({}).then(result => {
                return result
            })
        } else {
            return Promise.reject('Not authorized to access this page');
        }
    })
}


/* 
Create Order
1. An authenticated NON-admin user sends a POST request containing a JWT in its header to the /users/checkout endpoint.
2. API validates user identity via JWT, returns false if validation fails.
3. If validation successful, API creates order using the contents of the request body.
*/

module.exports.createOrder = async (reqBody, userData) => {

    let totalAmount = 0 
    let productsPurchased = [] 

    runLoop = async () => {
        let total = 0 

        for (let i = 0; i < reqBody.productsPurchased.length; i++ ) {

            await new Promise( resolve => setTimeout( resolve ) )
            await Product.findById(reqBody.productsPurchased[i].productId).then(result => {            
            
                if(reqBody.productsPurchased[i].quantity != undefined) {

                    let quantityCalculate = result.price * reqBody.productsPurchased[i].quantity
                    console.log(reqBody.productsPurchased[i].quantity)
                    total = quantityCalculate + total
                    productsPurchased.push({productId : result.id, price : result.price, quantity: reqBody.productsPurchased[i].quantity})
                } else { 
                    total = result.price + total
                    productsPurchased.push({productId : result.id, price : result.price})
                }

                if (i == reqBody.productsPurchased.length - 1) {
                    totalAmount = total
                }
            })
  
        }
  
        return true 
    }

    let isForLoopDone = await runLoop();

    if(isForLoopDone) {

        return User.findById(userData.id).then(result => {

            if (userData.isAdmin) { 
                return Promise.reject('Not authorized to access this page');
            } else { 
                let newOrder = new Order({
                    totalAmount: parseInt(totalAmount),
                    userId: userData.id,
                    productsPurchased: productsPurchased      
                })
                
                return newOrder.save().then((order, error) => {
                    return error ? false : 'Successfully ordered a product';
                }) 
            }
        }) 
    } else {
        return "error"
    }   
 }

/* 
Retrieve Authenticated User’s Orders
1. A GET request containing a JWT in its header is sent to the /users/myOrders endpoint.
2. API validates user is NOT an admin via JWT, returns false if validation fails.
3. If validation is successful, API retrieves orders belonging to authenticated user and returns them in its response.
*/
module.exports.getAllMyOrder = (userData) => {
    
    return User.findById(userData.id).then(result => {
        if (userData.isAdmin) {
            return Promise.reject('Not authorized to access this page');
            
        } else {

            return Order.find({userId: userData.id}).then(result => {
                return result
            })
            
        }
    })
}
