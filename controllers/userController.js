const User = require('../models/User.js')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

//Get all user
module.exports.getAllUser = (reqBody) => {
    return User.find({}).then(result => {
        return result
    })
}

/* 
    User registration
    Steps:
        1. Create a new User object using the mongoose model and the info from the request body.
        2. Make sure that the password is encrypted
        3. Save the new user to the database
*/

module.exports.registerUser = (reqBody) => {

    return User.findOne({ email:reqBody.email }).then(result => {
        if (result == null) {
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10),
            })
        
            //save
            return newUser.save().then((user, error) => {
                //if registration failed
                if(error) {
                    return "registration failed " + error;
                } else {
                    //user registration is successful
                    return "registration success";
                }
            })
        } else {
            return "user already exists"
        }
    })
}

/* 
    Login User
    Steps:
        1. Check if the user email exists in our database. If user does not exist, return false
        2. If the user exists, Compare the password provided in the login form with the password stored in the database
        3. Generate/return a jsonwebtoken if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

    return User.findOne({ email:reqBody.email }).then(result => {
        if (result == null) {
            return "User does not exist"
        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect){
                return { accessToken: auth.createAccessToken(result.toObject()) }
            } else {
                return "Wrong Password"
            }
        }
    })
}

/* 
    Set User As Admin
    Steps:
        1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
        2. API validates JWT, returns false if validation fails.
        3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.
*/

module.exports.setUserAsAdmin = (reqParams, userData) => {

    return User.findById(userData.id).then(result => {

        if (userData.isAdmin) {
            
            let newUserType = {
                isAdmin: true
            }
        
            return User.findByIdAndUpdate(reqParams.userId, newUserType).then((user, error) => {
                if(error) {
                    return false
                } else {

                    let newUserPreview = {
                        email: user.email,
                        isAdmin: true
                    }
                    return newUserPreview
                }
            })

        } else {
            return Promise.reject('Not authorized to access this page');
        }
        
    });    
}
