const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

//Routes for getting all user
router.get('/all', (req, res) => {
    userController.getAllUser().then(result => res.send(result))
})

//Routes for user registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
})

//Routes for authenticating a user
router.post('/login', (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result))
})

//Routes for set user as an admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.setUserAsAdmin(req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//

module.exports = router;