const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')

//Routes for getting all active product
router.get('/', (req, res) => {
    productController.getAllProduct().then(result => res.send(result))
})

//Routes for getting a product
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(result => res.send(result))
})

//Routes for creating a product, admin only
router.post('/', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    //productController.createProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result => res.send(result))
    productController.createProduct(req.body, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for updating a product, admin only
router.put('/:productId', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    //productController.createProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result => res.send(result))
    productController.updateProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for archiving a product, admin only
router.put('/:productId/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.archiveProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})

//Routes for archiving a product, admin only
router.put('/:productId/unarchive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    
    productController.unarchiveProduct(req.body, req.params, userData)
    .then(result => res.send(result))
    .catch(error => res.send(error))
})


module.exports = router;